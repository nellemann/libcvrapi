# libcvrapi

-----------------------------------

A Java library to query <https://www.cvrapi.dk> (a Danish VAT API service).

[![Build Status](https://drone.data.coop/api/badges/nellemann/libcvrapi/status.svg)](https://drone.data.coop/nellemann/libcvrapi)
## Usage

### Paid API

Lookup company by CVR number:

```java
CvrApi api = new CvrApi("My user agent", "mySecretToken");
Company company = api.getCompanyByVatNumber("57020415");
System.out.println(company.life.name);
```

The *Company* represents most of the same properties as seen in the example JSON in the [api documentation](http://docs.rest.cvrapi.dk/).


### Free API

The limited and free API can also be queried:

```java
OpenCvrApi api = new OpenCvrApi("My user agent");
OpenCompany company = api.getCompanyByVatNumber("57020415");
System.out.println(company.name);
```


### Gradle

    repositories {
		maven { url 'https://git.data.coop/api/packages/nellemann/maven' }
    }

    dependencies {
        compile 'biz.nellemann.libs:libcvrapi:1.+'
    }


## Development

To build and test the code:

    ./gradle build
    ./gradle test
