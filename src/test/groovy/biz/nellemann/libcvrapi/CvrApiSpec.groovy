package biz.nellemann.libcvrapi

import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import com.google.gson.JsonSyntaxException;
import spock.lang.Specification

class CvrApiSpec extends Specification {

    CvrApi api
	MockWebServer mockServer = new MockWebServer();

    def setup() {
		mockServer.start()
	}

    def cleanup() {
		mockServer.shutdown()
	}

	void "test unsuccessful parsing of JSON"() {
		setup:
		api = new CvrApi("Test User Agent", "testAuthToken")
		def testJson = "{'foo':'bar'}"

		when:
		def company = api.parseJsonIntoCompany(testJson)

		then:
		company.vat == null
	}

	void "test unsuccessful parsing of JSON w. VAT as text"() {
		setup:
		api = new CvrApi("Test User Agent", "testAuthToken")
		def testJson = "{'vat':'abcd1234'}"

		when:
		api.parseJsonIntoCompany(testJson)

		then:
		thrown Exception
	}

    void "test successful parsing company with CVR 25063864"() {

		setup:
		api = new CvrApi("Test User Agent", "testAuthToken")
		def testFile = new File(getClass().getResource('/25063864.json').toURI())
		def testJson = testFile.getText('UTF-8')

        when:
		def company = api.parseJsonIntoCompany(testJson)

        then:
        company != null
        company.vat == 25063864
        company.life.name == 'AGILLIC A/S'
        company.secondarynames.contains('Wavetech A/S')
		company.info.employment.amountEmployeesLow == 50
		company.info.employment.amountEmployeesHigh == 99
    }

	void "test successful parsing of company with CVR 15027800"() {

		setup:
		api = new CvrApi("Test User Agent", "testAuthToken")
		def testFile = new File(getClass().getResource('/15027800.json').toURI())
		def testJson = testFile.getText('UTF-8')

		when:
		def company = api.parseJsonIntoCompany(testJson)

		then:
		company != null
		company.vat == 15027800
		company.life.name == 'FREJA TRANSPORT & LOGISTICS A/S'
		company.secondarynames.contains('KT TRANSPORT A/S')
		company.info.employment.amountEmployeesLow == 200
		company.info.employment.amountEmployeesHigh == 499
	}

	void "test successful HTTP GET company with CVR 15027800 as JSON"() {

		setup:
		mockServer.enqueue(new MockResponse().setBody("{}"));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def jsonString = api.getCompanyJson("15027800")

		then:
		jsonString != null
		jsonString == "{}"
	}

	void "test we can HTTP GET company with CVR 25063864 deserialized into Company"() {

		setup:
		def testFile = new File(getClass().getResource('/25063864.json').toURI())
		def testJson = testFile.getText('UTF-8')
		mockServer.enqueue(new MockResponse().setBody(testJson));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def company = api.getCompanyByVatNumber("25063864")

		then:
		company != null
		company.vat == 25063864
	}

	void "test wrong json"() {
		setup:
		mockServer.enqueue(new MockResponse().setBody("{'foo':'bar'}"));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def company = api.getCompanyByVatNumber("15027800")

		then:
		company.vat == null
	}

	void "test empty json"() {
		setup:
		mockServer.enqueue(new MockResponse().setBody(''));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def company = api.getCompanyByVatNumber("15027800")

		then:
		company == null
	}

	void "test HTTP 404 reply"() {
		setup:
		mockServer.enqueue(new MockResponse().setResponseCode(404));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def company = api.get(baseUrl.toString())

		then:
		Exception ex = thrown()
		ex.message == 'Not Found'
	}

	void "test HTTP 401 reply"() {
		setup:
		mockServer.enqueue(new MockResponse().setResponseCode(401));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def company = api.get(baseUrl.toString())

		then:
		Exception ex = thrown()
		ex.message == 'Access Denied'
	}

	void "test other HTTP reply"() {
		setup:
		mockServer.enqueue(new MockResponse().setResponseCode(405));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def company = api.get(baseUrl.toString())

		then:
		Exception ex = thrown()
	}

	void "test we can HTTP GET company with CVR 39895692 deserialized into Company"() {

		setup:
		def testFile = new File(getClass().getResource('/39895692.json').toURI())
		def testJson = testFile.getText('UTF-8')
		mockServer.enqueue(new MockResponse().setBody(testJson));
		HttpUrl baseUrl = mockServer.url("/v1/dk/");
		api = new CvrApi("Test User Agent", "testAuthToken", baseUrl.toString())

		when:
		def company = api.getCompanyByVatNumber("39895692")

		then:
		company != null
		company.vat == 39895692
	}

	// TODO: Test 401, 404, timeouts, gson deserialization errors
}

