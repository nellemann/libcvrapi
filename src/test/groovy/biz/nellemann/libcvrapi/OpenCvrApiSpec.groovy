package biz.nellemann.libcvrapi

import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import com.google.gson.JsonSyntaxException;
import spock.lang.Specification

class OpenCvrApiSpec extends Specification {

	OpenCvrApi api
	MockWebServer mockServer = new MockWebServer();

	def setup() {
		mockServer.start()
	}

	def cleanup() {
		mockServer.shutdown()
	}

	void "test unsuccessful parsing of JSON"() {
		setup:
		api = new OpenCvrApi("Test User Agent")
		def testJson = "{'foo':'bar'}"

		when:
		def company = api.parseJsonIntoCompany(testJson)

		then:
		company.vat == null
	}

	void "test unsuccessful parsing of JSON w. VAT as text"() {
		setup:
		api = new OpenCvrApi("Test User Agent")
		def testJson = "{'vat':'abcd1234'}"

		when:
		api.parseJsonIntoCompany(testJson)

		then:
		thrown Exception
	}

	void "test successful parsing company with CVR 38979167"() {

		setup:
		api = new OpenCvrApi("Test User Agent")
		def testFile = new File(getClass().getResource('/open-38979167.json').toURI())
		def testJson = testFile.getText('UTF-8')

		when:
		def company = api.parseJsonIntoCompany(testJson)

		then:
		company != null
		company.vat == 38979167
		company.name == 'Mintr ApS'
	}

	void "test we can HTTP GET company with CVR 25063864 deserialized into Company"() {

		setup:
		def testFile = new File(getClass().getResource('/open-38979167.json').toURI())
		def testJson = testFile.getText('UTF-8')
		mockServer.enqueue(new MockResponse().setBody(testJson));
		HttpUrl baseUrl = mockServer.url("/api");
		api = new OpenCvrApi("Test User Agent", baseUrl.toString())

		when:
		def company = api.getCompanyByVatNumber("25063864")

		then:
		company != null
		company.vat == 38979167
	}

}

