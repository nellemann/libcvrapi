/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package biz.nellemann.libcvrapi;

import biz.nellemann.libcvrapi.pojo.Company;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;

public class CvrApi {

	private static final Logger log = LoggerFactory.getLogger(CvrApi.class);
	private final OkHttpClient client = new OkHttpClient();

	private final String userAgent;
	private final String authenticationToken;

	private final String BASE_URL;


	public CvrApi(String userAgent, String authenticationToken) {
		this(userAgent, authenticationToken, null);
	}

	public CvrApi(String userAgent, String authenticationToken, String baseUrl) {
		this.userAgent = userAgent;
		this.authenticationToken = authenticationToken;
		if(baseUrl == null) {
			this.BASE_URL = "https://rest.cvrapi.dk/v1/dk";
		} else {
			this.BASE_URL = baseUrl;
		}
	}


	protected String get(String url) throws Exception {

		String credential = Credentials.basic(authenticationToken, "");

		Request request = new Request
			.Builder()
			.url(url)
			.header("User-Agent", userAgent)
			.header("Authorization", credential)
			.addHeader("Accept", "application/json;")
			.build();

		try (Response response = client.newCall(request).execute()) {
			switch (response.code()) {
				case 200:
					return Objects.requireNonNull(response.body()).string();
				case 401:
					log.warn("get() - 401 - Access Denied");
					throw new Exception("Access Denied");
				case 404:
					log.warn("get() - 404 - Not Found");
					throw new Exception("Not Found");
				default:
					throw new Exception("get() - Unknown Error - status code: " + response.code());
			}
		}
	}


	/**
	 * Query the API: https://rest.cvrapi.dk/v1/dk/company/vatNumber
	 *
	 * @param vatNumber Danish VAT number
	 * @return String with JSON
	 */
	protected String getCompanyJson(String vatNumber) throws Exception {
		String response = get(BASE_URL + "/company/" + vatNumber);
		log.info("getCompanyJson() response: {}", response);
		return response;
	}


	/**
	 * Use GSON to deserialize JSON into POJO's.
	 *
	 * @param json String of JSON
	 * @return Company object
	 */
	protected Company parseJsonIntoCompany(String json) {
		Gson gson = new Gson();
		return gson.fromJson(json, Company.class);
	}


	/**
	 * Lookup company by VAT number
	 *
	 * @param vatNumber Danish VAT number as String
	 * @return Company object
	 */
	public Company getCompanyByVatNumber(String vatNumber) {

		try {
			String json = getCompanyJson(vatNumber);
			return parseJsonIntoCompany(json);
		} catch (JsonSyntaxException e) {
			log.error("Error parsing JSON", e);
			return null;
		} catch (IOException e) {
			log.error("IO Error", e);
			return null;
		} catch (Exception e) {
			log.error("Unknown Error", e);
			return null;
		}

	}

}
