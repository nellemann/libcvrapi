/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package biz.nellemann.libcvrapi.pojo;

public class Info {

    String articles_of_association;
    String purpose;
    String bind;
    /*
     * "modes": { "legislation_money_laundering": false, "social_economic": false,
     * "government": false }, "attributes": {
     * "management_recognized_as_beneficial_owners": true }, "demerges": null,
     * "merges": null, "lei": { "id": "894500WP4D8JDZ88S128" }, "employment": {
     * "intervalAmountEmployees": "ANTAL_50_99", "amountEmployeesLow": "50",
     * "amountEmployeesHigh": "99", "year": "2019", "month": "2" }
     */

    InfoEmployment employment;
}
