/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package biz.nellemann.libcvrapi.pojo;

import java.math.BigDecimal;
import java.math.BigInteger;

public class AccountingDocumentSummary {

	String start;
	String end;
	BigInteger revenue;
	BigInteger grossprofitloss;
	BigInteger profitloss;
	BigInteger equity;
	BigInteger averagenumberofemployees;
	BigInteger profitlossfromordinaryactivitiesbeforetax;
	BigInteger shorttermliabilitiesotherthanprovisions;
	BigInteger assets;
	BigInteger currentassets;
	BigInteger noncurrentassets;
	BigDecimal liquidityratio;
	BigDecimal solvencyratio;
	BigDecimal equityreturn;

	/* TODO: Add fields if needed
	"employeebenefitsexpense": "-27446000",
	"depreciationamortisationexpenseprofitorloss": null,
	"profitlossfromordinaryoperatingactivities": null,
	"incomefrominvestmentsingroupenterprises": null,
	"otherfinanceincome": null,
	"otherfinanceexpenses": null,
	"profitlossfromordinaryactivitiesbeforetax": null,
	"taxexpenseonordinaryactivities": null,
	"taxexpense": null,
	"proposeddividendrecognisedinequity": null,
	"proposeddividend": null,
	"dividend": null,
	"landandbuildings": null,
	"inventories": null,
	"shorttermtradereceivables": null,
	"cashandcashequivalents": "12312000",
	"equityloan": null,
	"provisions": null,
	"longtermliabilitiesotherthanprovisions": null,
	"liabilitiesandequity": null,
	"coverage": null,
	"operatingmargin": null,
	"roi": null,
 	*/
}


