/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package biz.nellemann.libcvrapi.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Company {

    BigDecimal vat;

    Life life;
    Address address;
    Contact contact;
    Industrycode industrycode;
    Companyform companyform;
    Companystatus companystatus;
    Status status;

    Accounting accounting;
    // Tax tax;

    Capital capital;
    Shareholder shareholder;
    Info info;

    ArrayList<String> secondarynames;

    // Ejerkredsen
    // List<Participant> participants

    // Ejerskab over andre selskaber
    // List<Participation> participations

}
