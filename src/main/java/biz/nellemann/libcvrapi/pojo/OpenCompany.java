package biz.nellemann.libcvrapi.pojo;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class OpenCompany {
	public BigDecimal vat;
	public String name;
	public String address;
	public String zipcode;
	public String city;

	@SerializedName("protected")
	public Boolean isProtected;

	public String phone;
	public String email;

	public String fax;
	public String startdate;
	public String enddate;

	public Integer industricode;
	public String industrydesc;

	public Integer companycode;
	public String companydesc;


}
