package biz.nellemann.libcvrapi;

import biz.nellemann.libcvrapi.pojo.Company;
import biz.nellemann.libcvrapi.pojo.OpenCompany;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;

// http://cvrapi.dk/api?search=' . $vat . '&country=' . $country);
public class OpenCvrApi {


	private static final Logger log = LoggerFactory.getLogger(OpenCvrApi.class);
	private final OkHttpClient client = new OkHttpClient();

	private final String userAgent;
	private final String BASE_URL;

	private final String country = "dk";	// TODO: Support NO, SE ?

	public OpenCvrApi(String userAgent) {
		this(userAgent, "http://cvrapi.dk/api");
	}

	public OpenCvrApi(String userAgent, String baseUrl) {
		this.userAgent = userAgent;
		if(baseUrl == null) {
			this.BASE_URL = "http://cvrapi.dk/api";
		} else {
			this.BASE_URL = baseUrl;
		}
	}


	protected String get(String url) throws Exception {

		Request request = new Request.Builder()
			.url(url)
			.addHeader("User-Agent", userAgent)
			.addHeader("Accept", "application/json;")
			.build();

		try (Response response = client.newCall(request).execute()) {
			switch (response.code()) {
				case 200:
					return Objects.requireNonNull(response.body()).string();
				case 401:
					log.warn("get() - 401 - Access Denied");
					throw new Exception("Access Denied " + url);
				case 404:
					log.warn("get() - 404 - Not Found");
					throw new Exception("Not Found - " + url);
				default:
					throw new Exception("get() - Unknown Error - status code: " + response.code());
			}
		}
	}


	/**
	 * Query the API:  http://cvrapi.dk/api?search=' . $vat . '&country=' . $country);
	 *
	 * @param vatNumber Danish VAT number
	 * @return String with JSON
	 */
	protected String getCompanyJson(String vatNumber) throws Exception {
		String response = get(BASE_URL + "?search=" + vatNumber + "&country=" + country);
		log.info("getCompanyJson() response: {}", response);
		return response;
	}


	/**
	 * Use GSON to deserialize JSON into POJO's.
	 *
	 * @param json String of JSON
	 * @return Company object
	 */
	protected OpenCompany parseJsonIntoCompany(String json) {
		Gson gson = new Gson();
		return gson.fromJson(json, OpenCompany.class);
	}


	/**
	 * Lookup company by VAT number
	 *
	 * @param vatNumber Danish VAT number as String
	 * @return Company object
	 */
	public OpenCompany getCompanyByVatNumber(String vatNumber) {

		try {
			String json = getCompanyJson(vatNumber);
			return parseJsonIntoCompany(json);
		} catch (JsonSyntaxException e) {
			log.error("Error parsing JSON", e);
			return null;
		} catch (IOException e) {
			log.error("IO Error", e);
			return null;
		} catch (Exception e) {
			log.error("Unknown Error", e);
			return null;
		}

	}
}
